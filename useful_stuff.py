class Directions():
    directions = {}    
    directions["N"] =   ([0, 1],    [0, 2],     [0, 3])
    directions["NE"] =  ([1, 1],    [2, 2],     [3, 3])
    directions["E"] =   ([1, 0],    [2, 0],     [3, 0])
    directions["SE"] =  ([1, -1],   [2, -2],    [3, -3])
    directions["S"] =   ([0, -1],   [0, -2],    [0, -3])
    directions["SW"] =  ([-1, -1],  [-2, -2],   [-3, -3])
    directions["W"] =   ([-1, 0],   [-2, 0],    [-3, 0])
    directions["NW"] =  ([-1, 1],   [-2, 2],     [-3, 3])