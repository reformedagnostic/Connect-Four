from random import randrange
from random import randint, randrange, shuffle
from useful_stuff import Directions

class ConnectFourRandom():

    def __init__(self, parent):
        self.parent = parent

    def make_move(self):
        valid_positions = self.qqarent.get_valid_pos()
        return_move = valid_positions[randrange(0, len(valid_positions))]
        return return_move

class ConnectFourAI(Directions):

    def __init__(self, parent):
        self.parent = parent
        self.button_dict = None

    def make_move(self):
        print("-------------")
        self.update_button_dict(self.parent)
        valid_positions = self.valid_moves()
        move = self.calc_move(valid_positions)
        print("move to:", move)
        if move == None:
            print("rnd")
            move = self.random_move(valid_positions)
        return move

    def update_button_dict(self, parent):
        self.button_dict = self.parent.button_dict

    def random_move(self, valid_positions_in):
        return_move = valid_positions_in[randrange(0, len(valid_positions_in))]
        return return_move

    def valid_moves(self):
        return_buttons = []
        counter = 0
        for button_id in self.button_dict:
            if counter == 7:
                break
            button = self.button_dict[button_id]
            x = button["x"]
            y = button["y"]
            if y == 0 and button["active"]:
                return_buttons.append(button_id)
                counter += 1
            elif button["active"] and self.button_dict[f"{x}/{y - 1}"]["active"] == False:
                return_buttons.append(button_id)
                counter += 1
        return return_buttons

    def calc_move(self, valid_moves):
        buttons = self.button_dict
        weight_max = 0
        return_move = None

        for key in buttons:
            if buttons[key]["active"] == True:
                continue
            owner = buttons[key]["player"]
            possible_fours, soft_fours, hard_fours = self.possible_fours(key, owner)
            print(possible_fours, soft_fours, hard_fours)
            weighed_moves = []
            if owner == "player1":
                if len(hard_fours) > 0:
                    hard_fours = self.read_move_list(hard_fours, valid_moves)
                    weighed_moves.append(self.weigh_moves(hard_fours, 100, 105))
                if len(soft_fours) > 0:
                    soft_fours = self.read_move_list(soft_fours, valid_moves)
                    weighed_moves.append(self.weigh_moves(soft_fours, 15, 20))
                if len(possible_fours) > 0:
                    possible_fours = self.read_move_list(possible_fours, valid_moves)
                    weighed_moves.append(self.weigh_moves(possible_fours, 1, 5))
            
            if owner == "player2":
                if len(hard_fours) > 0:
                    hard_fours = self.read_move_list(hard_fours, valid_moves)
                    weighed_moves.append(self.weigh_moves(hard_fours, 200, 205))
                if len(soft_fours) > 0:
                    soft_fours = self.read_move_list(soft_fours, valid_moves)
                    weighed_moves.append(self.weigh_moves(soft_fours, 10, 20))
                if len(possible_fours) > 0:
                    possible_fours = self.read_move_list(possible_fours, valid_moves)
                    weighed_moves.append(self.weigh_moves(possible_fours, 1, 5))

            for move in weighed_moves:
                print(move)
                if move[1] > weight_max:
                    return_move = move[0]
                    weight_max = move[1]

        return return_move


    def weigh_moves(self, list_in, weight_min, weight_max):
        shuffle(list_in)
        weight_return = 0
        return_move = None
        return_list = []
        for move in list_in:
            weight_tmp = randint(weight_min, weight_max)
            if weight_tmp > weight_return:
                return_move = move
                weight_return = weight_tmp
        return_list = [return_move, weight_return]
        return return_list

    def read_move_list(self, list_in, valid_moves):
        return_moves = []
        for moveset in list_in:
            for move in moveset:
                if move in valid_moves:
                    return_moves.append(move)
        return return_moves

    def possible_fours(self, key_in, owner_in):
        button = self.button_dict[key_in]
        x = button["x"]
        y = button["y"]
        possible_fours = []
        soft_fours = []
        hard_fours = []
        for dir in self.directions:
            fours_counter = 0
            tmp_fours = [f"{x}/{y}"]
            for i in range(0, 3):

                x_tmp = x + self.directions[dir][i][0]
                y_tmp = y + self.directions[dir][i][1]
                if x_tmp < 0 or x_tmp > 6:
                    break
                if y_tmp < 0 or y_tmp > 5:
                    break
                tmp_coord = f"{x_tmp}/{y_tmp}"
                tmp_button = self.button_dict[tmp_coord]
                
                if tmp_button["player"] == owner_in:
                    fours_counter += 1
                    tmp_fours.append(tmp_coord)
                elif tmp_button["player"] == None:
                    tmp_fours.append(tmp_coord)
                else:
                    break
                
                if i == 2:
                    if fours_counter == 0:
                        possible_fours.append(tmp_fours)
                    if fours_counter == 1:
                        soft_fours.append(tmp_fours)
                    if fours_counter == 2:
                        hard_fours.append(tmp_fours)
        
        return possible_fours, soft_fours, hard_fours
