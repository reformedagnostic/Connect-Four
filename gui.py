import tkinter as tk
from tkinter import messagebox
from ai import ConnectFourAI, ConnectFourRandom
from useful_stuff import Directions

class Game(tk.Tk, Directions):
    def __init__(self):
        tk.Tk.__init__(self)
        self.current_game = None
        self.button_dict = None
        self.active_player = "player1"
        self.create_game()

    def create_game(self):
        if self.current_game is not None:
            self.current_game.destroy()
        self.current_game = Gameboard(self)
        self.current_game.pack(expand=True)
        self.computer_player = ConnectFourAI(self)

    def get_valid_pos(self):
        return_positions = []
        for position in self.current_game.button_dict:
            if self.current_game.check_position_valid(position):
                return_positions.append(position)
        return return_positions

    def update_button_dict(self):
        self.button_dict = self.current_game.button_dict

    def switch_player(self):
        if self.active_player == "player1":
            self.update_button_dict()
            the_end = self.check_winner()
            if not the_end:
                self.active_player = "player2"
                move = self.computer_player.make_move()
                self.current_game.button_click(move)
        elif self.active_player == "player2":
            self.update_button_dict()
            self.check_winner()
            self.active_player = "player1"

    def check_winner(self):
        buttons = self.current_game.button_dict
        for key in buttons:
            if buttons[key]["active"] == True:
                continue
            if buttons[key]["player"] != self.active_player:
                continue
            if self.check_winner_directions(key):
                message = messagebox.showinfo("Fin", f"{self.active_player} wins.")
                self.create_game()
                return True
        return False

    def check_winner_directions(self, key_in):
        buttons = self.current_game.button_dict[key_in]
        x = buttons["x"]
        y = buttons["y"]
        for dir in self.directions:
            for i in range(0, 3):
                x_tmp = x + self.directions[dir][i][0]
                y_tmp = y + self.directions[dir][i][1]
                if x_tmp < 0 or x_tmp > 6:
                    break
                if y_tmp < 0 or y_tmp > 5:
                    break
                if self.current_game.button_dict[f"{x_tmp}/{y_tmp}"]["player"] == self.active_player:
                    if i == 2:
                        return True
                    continue
                else:
                    break

class Gameboard(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.neutral_button = tk.PhotoImage(file="images/grey_button.png")
        self.player1_button = tk.PhotoImage(file="images/light_petrol_button.png")
        self.player2_button = tk.PhotoImage(file="images/orange_button.png")
        self.button_dict = {}
        for y in range(5, 0 - 1, -1):
            for x in range(0, 6 + 1, 1):
                button_id = f"{x}/{y}"
                self.button_dict[button_id] = {}
                button = self.button_dict[button_id]
                button["x"] = x
                button["y"] = y
                button["active"] = True
                button["player"] = None
                button["button"] = tk.Button(self, image=self.neutral_button)
                button["button"].configure(command = lambda id = button_id: self.button_click(id))
                button["button"].configure(bg="white")
                button["button"].configure(border="0")
                button["button"].grid(column=x, row=6-y)

        self.active_player = "player1"

    def button_click(self, button_id):
        if self.check_position_valid(button_id):
            button = self.button_dict[button_id]
            if self.parent.active_player == "player1" and button["active"] == True:
                button["button"].configure(image=self.player1_button)
                button["active"] = False
                button["player"] = "player1"
                self.parent.switch_player()

            elif self.parent.active_player == "player2" and button["active"] == True:
                button["button"].configure(image=self.player2_button)
                button["active"] = False
                button["player"] = "player2"
                self.parent.switch_player()

    def check_position_valid(self, button_id):
        button = self.button_dict[button_id]
        x = button["x"]
        y = button["y"]
        if y == 0 and button["active"]:
            return True
        elif button["active"] and self.button_dict[f"{x}/{y - 1}"]["active"] == False:
            return True
        return False



        
